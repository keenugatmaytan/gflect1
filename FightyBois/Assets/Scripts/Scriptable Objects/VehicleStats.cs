﻿using UnityEngine;


[CreateAssetMenu(fileName = "Kart Statistics", menuName = "ScriptableObjects/Kart Stats", order = 1)]
public class VehicleStats : ScriptableObject
{
    [SerializeField]
    [Range(20, 100)]
    public float accel = 20; //Acceleration Value
    public AnimationCurve accelCurve;
    [SerializeField]
    [Range(20, 50)]
    public float topSpeed = 20; //Max Forward Speed
    [SerializeField]
    public float reverseSpeed; // Max Backward Speed
    [SerializeField]
    [Range(45, 60)]
    public float turningRate = 45; //Turning Rate (How much the object will rotate along the y axis per physics second)
}
