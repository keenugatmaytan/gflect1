﻿using UnityEngine;

[CreateAssetMenu(fileName = "Abilities", menuName = "ScriptableObjects/Abilites")]
public class Abilities : ScriptableObject
{

    public void ReduceAccel(Mine bomb, Transform transform)
    {
        Debug.Log("Slow Down!");
        //Leaves behind a projectile for other karts to hit
        //Set bomb mesh to use Kapre bomb mesh
        bomb.transform.position = transform.position + transform.forward * -2.5f;
        bomb.GetComponent<Mine>().racer = 1;
        bomb.gameObject.SetActive(true);
    }

    public void SpinAttack(Mine bomb, Transform transform)
    {
        Debug.Log("You Spin me right round");
        //Play an animation
        //Leave projectile for other racers to hit
        //Raise racer above the ground then spin 360
        //Set bomb mesh to use Duwende bomb mesh
        bomb.transform.position = transform.position + transform.forward * -5f;
        bomb.GetComponent<Mine>().racer = 0;
        bomb.gameObject.SetActive(true);
    }

    public void SmokeScreen(int charge, int cost)
    {
        //Kapre Ult
        //Implement Cost system
        if (charge >= cost)
        {
            Debug.Log("Smoke This!");
            //Particle System
            //Smoke Cloud around racer for 10 seconds
            //Smoke Trail for 30 seconds
        }
    }

    public void ResetCoolDowns(AIController aIController, int charge, int cost)
    {
        AIController[] cars = GameObject.FindObjectsOfType<AIController>();
        if (charge >= cost)
        {
            Debug.Log(aIController + "Activated their ult");
            aIController.essAmt -= cost;
            //Reduce aicontroller.charge amount by cost amount
            for (int i = 0; i < cars.Length; i++) //Get a list of all AIControllers in the game
            {
                Debug.Log(cars[i].name);
                if (cars[i].tag != "Player") //Make sure we don't affect ourself
                {
                    GameObject.FindObjectsOfType<AIController>()[i].GetComponent<AIController>().abilityCool = 3; //Set abilityCool to 3 which makes all abilities wait for 3 seconds before activating
                }
            }
        }
    }
}
