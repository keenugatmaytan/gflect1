﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ManageVolume : MonoBehaviour
{
    public AudioMixer myMixer;
    public string groupName;

    public void setVolumeLevel(float slider) {
        myMixer.SetFloat(groupName, Mathf.Log10(slider) * 20);
    }
}
