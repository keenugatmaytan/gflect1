﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonChangeSpriteUponSelection : MonoBehaviour
{
    public Image myUIImage;
    public Sprite[] myUIButtons;
    public GameObject downArrow;

    private void Update()
    {
        
    }

    public void mouseOver()
    {
        myUIImage.sprite = myUIButtons[1];
    }

    public void mouseExit()
    {
        myUIImage.sprite = myUIButtons[0];
    }

    public void selected() {
        if (downArrow != null) {
            downArrow.SetActive(true);
        }
    }

    public void deselect() {
        if (downArrow != null)
        {
            downArrow.SetActive(false);
        }
    }

    private void OnMouseDown()
    {
        myUIImage.sprite = myUIButtons[1];
    }
}
