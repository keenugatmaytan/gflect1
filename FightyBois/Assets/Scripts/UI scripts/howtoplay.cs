﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class howtoplay : MonoBehaviour
{
    public string[] tips;
    public Text theTips;
    public GameObject left, right, back;
    public Sprite[] imageHolder;
    public Image mainImage;
    int tipNum = 0;

    private void OnEnable()
    {
        tipNum = 0;
        changeTips(tipNum);
    }

    public void changeTips(int i)
    {     
        if (tipNum + i != -1 && tipNum + i != 7) {
            tipNum += i;
            theTips.text = tips[tipNum];                              
        }

        if (tipNum == 1)
        {
            mainImage.gameObject.SetActive(true);
            mainImage.sprite = imageHolder[0];
        }
        else if (tipNum == 2)
        {
            mainImage.sprite = imageHolder[1];
        }
        else if (tipNum == 3)
        {
            mainImage.sprite = imageHolder[2];
        }
        else if (tipNum == 4)
        {
            mainImage.sprite = imageHolder[3];
        }
        else if (tipNum == 5)
        {
            mainImage.sprite = imageHolder[4];
        }
        else if (tipNum == 6)
        {
            mainImage.sprite = imageHolder[5];
        }
        else
            mainImage.gameObject.SetActive(false);
    }

}
