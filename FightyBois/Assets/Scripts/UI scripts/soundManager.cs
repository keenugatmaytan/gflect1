﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour
{
    public AudioClip[] myAudio;
    public AudioSource mySource;

    public void playSound(string i) {
        switch (i) {
            case "clicked":
                mySource.PlayOneShot(myAudio[0]);
                break;
            case "confirm":
                mySource.PlayOneShot(myAudio[1]);
                break;
            case "select":
                mySource.PlayOneShot(myAudio[2]);
                break;
            case "dwende":
                mySource.PlayOneShot(myAudio[3]);
                break;
            case "kapre":
                mySource.PlayOneShot(myAudio[4]);
                break;


        }
    }
    
}
