﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterConfirmSettings : MonoBehaviour
{
    public uiManager myUI;
    public selectCharacter myPlayers;
    public bool raceSettingsOn = false;

    private void OnEnable()
    {
        gamemanager.instance.playersPicked = 0;
        myPlayers.picked = false;
    }

    // Update is called once per frame
    void Update()
    {
        allPlayersPicked();
    }

    void allPlayersPicked() {
        if (gamemanager.instance.players == gamemanager.instance.playersPicked && raceSettingsOn == false) {
            myUI.turnOnRaceSettings(5);
            raceSettingsOn = true;
        }
    }
}
