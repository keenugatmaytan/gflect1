﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class loadLeaderboards : MonoBehaviour
{
    public TextMeshProUGUI[] _name, charName, time;
    public Data myData;

    private void OnEnable()
    {
        loadData();
    }

    void loadData() {
        if (gamemanager.instance.selectedMap == 2)
        {
            for (int i = 0; i < gamemanager.instance.beachLeaderboard.Count; i++)
            {
                _name[i].text = "Player 1";

                if (gamemanager.instance.beachLeaderboard[i].character == 0)
                {
                    charName[i].text = "Dwende";
                }
                else
                    charName[i].text = "Kapre";

                DisplayTime(gamemanager.instance.beachLeaderboard[i].time, i);
            }
        }
        else
        {
            for (int i = 0; i < gamemanager.instance.lakeLeaderboard.Count; i++)
            {
                _name[i].text = "Player 1";

                if (gamemanager.instance.lakeLeaderboard[i].character == 0)
                {
                    charName[i].text = "Dwende";
                }
                else
                    charName[i].text = "Kapre";

                DisplayTime(gamemanager.instance.lakeLeaderboard[i].time, i);
            }
        }
    }

    public void DisplayTime(float timeToDisplay, int i)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliSeconds = (timeToDisplay % 1) * 1000;

        time[i].text = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliSeconds);
        
    }
}
