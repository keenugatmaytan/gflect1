﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapSelect : MonoBehaviour
{
    public int currentMap, currentPreview;
    public TextMeshProUGUI frontText, backText;
    public Image beachMountain;
    public Sprite[] beachPics, lakePics;
    public uiManager myUIM;
    public int mapName;
    public sceneManager mySCeneM;
    public GameObject selectorleft, selectorright;

    private float timer = 3, cd = 3;

    private void OnEnable()
    {
        if (gamemanager.instance.raceMode)
        {
            selectorleft.SetActive(false);
            selectorright.SetActive(false);
        }
        else
        {
            selectorleft.SetActive(true);
            selectorright.SetActive(true);
        }

        currentMap = 0;
        changeMapVisual();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) {
            selectMap();
        }

        changePreviewImage(currentMap);
    }

    public void changeMapButton(int i) {
        if (i + currentMap != 2 && i - currentMap != -1) {
            currentMap += i;
            changeMapVisual();
        }
    }

    void changeMapVisual() {
        if (currentMap == 0)
        {
            frontText.text = "Beach Mountain";
            backText.text = "Beach Mountain";
            mapName = 2;
        }
        else
        {
            frontText.text = "Lake";
            backText.text = "Lake";
            mapName = 3;
        }
    }

    public void selectMap() {
        gamemanager.instance.selectedMap = mapName;
        mySCeneM.loadTrack(gamemanager.instance.selectedMap);
    }

    void changePreviewImage(int i) {
        if (timer <= 0)
        {
            ++currentPreview;
            if (currentPreview == 3) {
                currentPreview = 0;
            }
            timer = cd;
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        if (i == 0)
        {
            if (currentPreview == 0)
            {
                beachMountain.sprite = beachPics[0];
            }
            else if (currentPreview == 1)
            {
                beachMountain.sprite = beachPics[1];
            }
            else if (currentPreview == 2)
            {
                beachMountain.sprite = beachPics[2];
            }
        }
        else
        {
            if (currentPreview == 0)
            {
                beachMountain.sprite = lakePics[0];
            }
            else if (currentPreview == 1)
            {
                beachMountain.sprite = lakePics[1];
            }
            else if (currentPreview == 2)
            {
                beachMountain.sprite = lakePics[2];
            }
        }

   
    }
}
