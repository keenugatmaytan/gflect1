﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sceneChangeButton : MonoBehaviour
{
    public uiManager myUI;
    public int nextScn, prevScn;
    // Update is called once per frame
    void Update()
    {
        nextScene();
        previousScene();
    }

    public void nextScene() {
        if (Input.GetKeyDown(KeyCode.Return)) {
            gamemanager.instance.updateCharSize();
            myUI.turonUI(nextScn);
        }
    }

    public void previousScene()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            myUI.turonUI(prevScn);
        }
    }
}
