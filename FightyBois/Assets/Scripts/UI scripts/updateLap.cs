﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class updateLap : MonoBehaviour
{
    public TextMeshProUGUI lapText, backText, numOflaps, numOflapsback;
    public int maxlaps;

    private void Start()
    {
        maxlaps = gamemanager.instance.laps;
        numOflaps.text = "/" + maxlaps;
        numOflapsback.text = "/" + maxlaps;
    }

    public void updateText(int currentlap) {
        lapText.text = currentlap.ToString();
        backText.text = currentlap.ToString();
    }
}
