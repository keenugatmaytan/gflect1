﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectCharacter : MonoBehaviour
{
    public GameObject[] selectorP1;
    public PlayerControllerNumber myPlayerNum;
    public DualShock4Input myController;
    public bool picked;
    public KeyCode myKey;
    public soundManager mySound;
    int i = 0;
    public uiManager myUI;

    private void OnEnable()
    {
        picked = false;
        gamemanager.instance.playersPicked = 0;
    }

    void Update()
    {
        if (!picked)
        {
            charP1();
            selectCharP1();
        }          
    }

    void charP1() {

        if (myController != null) {
            if (myController.GetAxisRaw(ControlCode.LeftStickX) < 0)
            {
                if (i > 0)
                {
                    selectorP1[i].SetActive(false);
                    i -= 1;
                    selectorP1[i].SetActive(true);
                }
            }

            if (myController.GetAxisRaw(ControlCode.LeftStickX) > 0)
            {
                if (i < 1)
                {
                    selectorP1[i].SetActive(false);
                    i += 1;
                    selectorP1[i].SetActive(true);
                }
            }
        }      

        if (Input.GetKeyDown((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left", "LeftArrow"))))
        {
            if (i > 0)
            {
                selectorP1[i].SetActive(false);
                i -= 1;
                selectorP1[i].SetActive(true);
            }
        }


        if (Input.GetKeyDown((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right", "RightArrow"))))
        {
            if (i < 1)
            {
                selectorP1[i].SetActive(false);
                i += 1;
                selectorP1[i].SetActive(true);
            }
        }
    }

    void selectCharP1() {
        if (myController != null)
        {
            if (myController.GetButtonDown(ControlCode.X))
            {
                gamemanager.instance.charSelect[(int)myPlayerNum - 1] = i;
                if (gamemanager.instance.charSelect[0] == 0)
                {
                    mySound.playSound("dwende");
                }
                else
                    mySound.playSound("kapre");
                gamemanager.instance.playersPicked++;
                picked = true;
                allPlayersPicked();
            }
        }
        else {
            if (Input.GetKeyDown(myKey))
            {
                gamemanager.instance.charSelect[(int)myPlayerNum - 1] = i;
                if (gamemanager.instance.charSelect[0] == 0)
                {
                    mySound.playSound("dwende");
                }
                else
                    mySound.playSound("kapre");
                gamemanager.instance.playersPicked++;
                picked = true;
                allPlayersPicked();
            }
        }
    }

    public void clickNextButton() {
        gamemanager.instance.charSelect[(int)myPlayerNum - 1] = i;
        if (gamemanager.instance.charSelect[0] == 0) {
            mySound.playSound("dwende");
        } else
            mySound.playSound("kapre");
        gamemanager.instance.playersPicked++;
        picked = true;
        allPlayersPicked();
    }

    public void buttonSelect(int j) {
        if (j ==  1)
        {
            
            selectorP1[i].SetActive(false);
            i = j;
            selectorP1[i].SetActive(true);
        }
        else
        {
            selectorP1[i].SetActive(false);
            i = j;
            selectorP1[i].SetActive(true);
        }
    }

    void allPlayersPicked()
    {
        if (gamemanager.instance.players == gamemanager.instance.playersPicked)
        {
            myUI.turnOnRaceSettings(5);    
        }
    }
}
