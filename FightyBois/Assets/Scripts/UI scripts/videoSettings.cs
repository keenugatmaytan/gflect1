﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class videoSettings : MonoBehaviour
{
    public Dropdown resoOptions;
    Resolution[] resolutions;
    int currentReso = 0;

    void Start()
    {
        resolutions = Screen.resolutions;
        resoOptions.ClearOptions();

        List<string> resoList = new List<string>();

        for (int i = 0; i < resolutions.Length; i++) {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            resoList.Add(option);

            if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height) {
                currentReso = i;
            }
        }

        resoOptions.AddOptions(resoList);
        resoOptions.value = currentReso;
        resoOptions.RefreshShownValue();
    }

    public void setScreenMode(int screenModeIndex) {
        if (screenModeIndex == 0) {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }

        if (screenModeIndex == 1) {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }

        if (screenModeIndex == 2) {
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        }
    }

    public void setReso(int resoindex) {
        Resolution myreso = resolutions[resoindex];
        Screen.SetResolution(myreso.width, myreso.height, Screen.fullScreenMode);
    }
}
