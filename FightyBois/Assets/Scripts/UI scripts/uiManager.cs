﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiManager : MonoBehaviour
{
    public GameObject[] UIs;
    public int currentActiveUI;
    public int previousUI;
    public selectCharacter script2;

    public void turonUI(int i) {
        UIs[currentActiveUI].SetActive(false);
        UIs[i].SetActive(true);
        currentActiveUI = i;

        if (UIs[5].activeInHierarchy) {
            UIs[5].SetActive(false);
        }
    }

    public void optionsUI(int i) {
        if (i == 2) {
            UIs[2].SetActive(true);
            UIs[3].SetActive(false);
            UIs[4].SetActive(false);
        }

        if (i == 3)
        {
            UIs[2].SetActive(false);
            UIs[3].SetActive(true);
            UIs[4].SetActive(false);
        }

        if (i == 4)
        {
            UIs[2].SetActive(false);
            UIs[3].SetActive(false);
            UIs[4].SetActive(true);
        }
    }

    public void turnOffRaceSettings(int i) {
        UIs[i].SetActive(false);
        script2.picked = false;
        gamemanager.instance.playersPicked = 0;
    }

    public void turnOnRaceSettings(int i)
    {
        UIs[i].SetActive(true);
    }
}
