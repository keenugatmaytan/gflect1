﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class updatePosition : MonoBehaviour
{
    public  TextMeshProUGUI lapText, backText, theTH, backTheTH;
    private string finalPos;

    public void updatePos(int currentPos)
    {
        string name;
        if (currentPos == 1)
        {
            name = "ST";
        }
        else if (currentPos == 2)
        {
            name = "ND";
        }
        else if (currentPos == 3)
        {
            name = "RD";
        }
        else
        {
            name = "TH";
        }
        lapText.text = currentPos.ToString();
        theTH.text = name;
        backText.text = currentPos.ToString();
        backTheTH.text = name;
    }
}
