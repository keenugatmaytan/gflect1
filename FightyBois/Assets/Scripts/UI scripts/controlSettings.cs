﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class controlSettings : MonoBehaviour
{
    private Dictionary<string, KeyCode> keybind = new Dictionary<string, KeyCode>();
    public Text[] P1binds;
    private GameObject currentKey;

    // Start is called before the first frame update
    void Start()
    {
        keybind.Add("forward", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("forward", "C")));
        keybind.Add("left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left", "LeftArrow")));
        keybind.Add("right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right", "RightArrow")));
        keybind.Add("reverse", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("reverse", "X")));
        keybind.Add("drift", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("drift", "Z")));
        keybind.Add("ult", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ult", "Space")));
        keybind.Add("A1", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A1", "A")));
        keybind.Add("A2", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A2", "S")));

        P1binds[0].text = keybind["forward"].ToString();
        P1binds[1].text = keybind["left"].ToString();
        P1binds[2].text = keybind["right"].ToString();
        P1binds[3].text = keybind["reverse"].ToString();
        P1binds[4].text = keybind["drift"].ToString();
        P1binds[5].text = keybind["ult"].ToString();
        P1binds[6].text = keybind["A1"].ToString();
        P1binds[7].text = keybind["A2"].ToString();
    }

    private void OnGUI()
    {
        if (currentKey != null) {
            Event e = Event.current;

            if (e.isKey) {
                keybind[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
            }
        }
    }

    public void getkey(GameObject obj) {
        currentKey = obj;
    }

    public void saveControls() {
        foreach (var key in keybind) {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }

        PlayerPrefs.Save();
    }
}
