﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class settingsValue : MonoBehaviour
{
    public Text laps;
    public Text abilities;

    private void OnEnable()
    {
       
        gamemanager.instance.laps = 1;
        gamemanager.instance.abilities = true;
        laps.text = gamemanager.instance.laps.ToString();

        if (gamemanager.instance.raceMode == true)
        {
            abilities.text = "Race";
        }
        else
            abilities.text = "Time Trial";
    }

    public void changeLapsValue(int i)
    {
        if (gamemanager.instance.laps + i != 0 && gamemanager.instance.laps + i != 4)
            gamemanager.instance.laps += i;

        laps.text = gamemanager.instance.laps.ToString();
    }

    public void toggleAbilities(bool i) {
        gamemanager.instance.raceMode = i;

        if (gamemanager.instance.raceMode == true)
        {
            abilities.text = "Race";
        }
        else
            abilities.text = "Time Trial";
    }

}
