﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openPause : MonoBehaviour
{
    public GameObject pauseUI;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (pauseUI.activeInHierarchy)
            {
                pauseUI.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                pauseUI.SetActive(true);
                Time.timeScale = 0;
            }             
        }
    }

    public void resume() {
        pauseUI.SetActive(false);
        Time.timeScale = 1;
    }
}
