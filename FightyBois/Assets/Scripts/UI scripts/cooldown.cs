﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class cooldown : MonoBehaviour
{
    public TextMeshProUGUI cooldownText;
    public Image myImg;
    public float cdAmount;

    // Update is called once per frame
    void Update()
    {
        if (cdAmount > 0)
        {
            cooldownText.enabled = true;
            cdAmount -= Time.deltaTime;
            cooldownText.text = Mathf.RoundToInt(cdAmount).ToString();
            myImg.color = Color.gray;
        }
        else
        {
            cooldownText.enabled = false;
            myImg.color = Color.white;
        }
            
    }


}
