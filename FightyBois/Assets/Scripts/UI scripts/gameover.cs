﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class gameover : MonoBehaviour
{
    public Checkpoints topFourKarts;
    public TextMeshProUGUI[] myText;

    private void OnEnable()
    {
        takeTopFour();
    }

    void takeTopFour() {
        for (int i = 0; i < 8; i++) {
            myText[i].text = topFourKarts.player[i].name;
        }
    }
}
