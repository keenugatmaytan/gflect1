﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeSkillImage : MonoBehaviour
{
    public Sprite[] dwendeSkills, kapreSkills;
    public Image[] playerSkills;

    void Start()
    {
        if (gamemanager.instance.charSelect[0] == 0)
        {
            for (int i = 0; i < 4; i++)
            {
                playerSkills[i].sprite = dwendeSkills[i];
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                playerSkills[i].sprite = kapreSkills[i];
            }
        }
    }
}
