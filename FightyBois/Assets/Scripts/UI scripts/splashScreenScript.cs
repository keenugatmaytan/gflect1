﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class splashScreenScript : MonoBehaviour
{
    public bool canClick = false;

    public void Start()
    {
        StartCoroutine(clickCooldown());
    }

    void Update()
    {
        if (Input.anyKey) {
            SceneManager.LoadScene("Title Screen");
        }
    }

    private IEnumerator clickCooldown() {
        yield return new WaitForSeconds(1.5f);
        canClick = true;
    }
}
