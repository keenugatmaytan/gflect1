﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data
{
    public float[] time = new float[16];
    public int[] character = new int[16];
}
