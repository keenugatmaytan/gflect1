﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField]
    AIController kartReference;
    [SerializeField]
    Text speed;

    public GameObject[] UI;

    // Start is called before the first frame update
    void Start()
    {
        if (kartReference == null)
        {
            kartReference = FindObjectOfType<AIController>();
        }
        turnOffAbility();
        //speed.text = Mathf.Floor(kartReference.curSpeed).ToString();

    }

    // Update is called once per frame
    void Update()
    {
        //speed.text = Mathf.Floor(kartReference.curSpeed).ToString();
    }

    void turnOffAbility() {

        if (!gamemanager.instance.raceMode)
        {
            UI[0].gameObject.SetActive(true);
        } else
            UI[1].gameObject.SetActive(true);
    }
}
