﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEditor;
//using UnityEditor.Tilemaps;
using UnityEngine;
using UnityEngine.UIElements;

public class AntirollAI : MonoBehaviour
{
    [SerializeField]
    GameObject[] tires;
    [SerializeField]
    bool[] tireGround;
    [SerializeField]
    AIController aiController;
    [SerializeField]
    Transform kart;
    [SerializeField]
    float neutralGround;
    [SerializeField]
    float upDiff;
    [SerializeField]
    float nDiff;

    private void Start()
    {
        tireGround = new bool[tires.Length];
        for (int i = 0; i < tireGround.Length; i++)
        {
            tireGround[i] = tires[i];
            tireGround[i] = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in tireGround)
        {
            if (!item)
            {
                aiController.grounded = false;
            }
            else
            {
                aiController.grounded = true;
                neutralGround = kart.eulerAngles.z;
            }
        }
        CheckGround();
    }

    void CheckGround()
    {
        for (int i = 0; i < tires.Length; i++)
        {
            RaycastHit hit;
            Ray groundRay = new Ray(tires[i].transform.position, -tires[i].transform.up * .5f);
            Debug.DrawRay(tires[i].transform.position, -tires[i].transform.up * .5f);
            if (Physics.Raycast(groundRay, out hit, .5f))
            {
                //Debug.Log(tires[i].name + " is Grounded.");
                tireGround[i] = true;
            }
            else
            {
                tireGround[i] = false;
                RotateToZero();
            }

        }

    }

    void RotateToZero()
    {

    }

}

