﻿using UnityEngine;

//Current Issues
//1. 

public class KartController : MonoBehaviour
{
    #region Variables

    #region Kart Components
    [Header("Kart Components")]
    [SerializeField]
    Rigidbody kart; //Reference to main kart object's rigidbody
    [SerializeField]
    Transform kartBody; //Reference to kart model's Transform
    [SerializeField]
    Transform[] frontWheels; //Reference to front wheels for visual purposes (Not needed for current design)
    [SerializeField]
    Transform frontAxle; //Reference to Front Axle Object
    public Animator characterAnimator; //Reference to character animator (Not needed for current design)
    #endregion

    #region Kart Specs Variables
    [SerializeField]
    VehicleStats vStats;
    [SerializeField]
    bool isForward; //Direction Value
    [SerializeField]
    public bool grounded = false; //Determines if the car is grounded and oriented properly (Checked in Anti Roll Script)
    [SerializeField]
    [Range(20, 100)]
    float accel = 20; //Acceleration Value
    [SerializeField]
    float accelMod; //Boost Value (Gets added after a drift)
    [SerializeField]
    float count = 3; //Countdown for Boost Decay (Decay Boost after 3(or count) seconds
    [SerializeField]
    [Range(20, 50)]
    float topSpeed = 20; //Max Forward Speed
    [SerializeField]
    float reverseSpeed; // Max Backward Speed
    [SerializeField]
    public float curSpeed; //Current Speed
    [SerializeField]
    [Range(45, 60)]
    float turningRate = 45; //Turning Rate (How much the object will rotate along the y axis per physics second)
        #endregion

    #region Inputs
    [SerializeField]
    float verticalInput, horizontalInput; //Floats to hold Input (-1 / 1 values)
    [SerializeField]
    bool drifting = false; //Boolean to check if the kart is drifting
    [SerializeField]
    bool reversing = false; //Boolean to check if the kart is reversing
    #endregion

    #region Debug
    [Header("Debug")]
    [SerializeField]
    float kartBodyY;
    [SerializeField]
    float kartRBY;
    [SerializeField]
    float difference;
    [SerializeField]
    float angle;
    [SerializeField]
    Transform resetPosition;
    #endregion

    #endregion

    #region Unity Functions
    // Start is called before the first frame update
    void Start()
    {
        kart = this.GetComponent<Rigidbody>();
        Reset();
        reverseSpeed = topSpeed * .35f;
    }

    // Update is called once per frame
    void Update()
    {
        angle = Vector3.Angle(kart.transform.forward, frontAxle.forward);
        curSpeed = kart.velocity.magnitude;
        
        //Take Input from player
        verticalInput = Input.GetAxis("Vertical");
        horizontalInput = Input.GetAxis("Horizontal");
            
        //Reset Button and Functionality
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reset();
        }


        //isForward Logic Checks if the player is trying to move forward by pressing w/ forward input 
        if (Input.GetKey(KeyCode.W))
        {
            isForward = true;
        }
        else { isForward = false; }
        //Debug.Log(Vector3.Angle(kart.transform.forward, frontAxle.forward));

    }
    private void FixedUpdate()
    {
        if (grounded)
        {
            Accelerate();
            Brakes();
            Turn();
            Reverse();
        }
        Attack();

        //The Car should auto reset to facing forward after drifting (Reset kartBody's forward to be the same as kart's forward)
        if (difference > 0 && !drifting)
        {
            kartBody.Rotate(new Vector3(0, kart.transform.eulerAngles.y - kartBody.eulerAngles.y), 45 * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            //drifting = true;
            //Drift();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            drifting = false;
            Boost();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(kartBody.position, -kartBody.up);
    }
    #endregion

    #region Custom Functions

    private void Reset()
    {
        this.transform.position = resetPosition.position;
        this.transform.rotation = resetPosition.rotation;
        kartBody.rotation = resetPosition.rotation;
        kart.velocity = Vector3.zero;
    }

    //Method to Accelerate
    void Accelerate()
    {
        //Add force to rigidbody
        kart.AddForce(transform.forward * (accelMod), ForceMode.Acceleration); //Add Boost regardless of input
        if (curSpeed <= topSpeed && verticalInput > 0)
        {
            kart.AddForce(transform.forward * (accel), ForceMode.Acceleration);
        }

        {
            if (accelMod > 0 && !drifting)
            {
                count -= Time.deltaTime;
                if (count <= 0) 
                {
                    accelMod -= Time.deltaTime;
                }

                if (accelMod < 0)
                {
                    count = 3;
                    accelMod = 0;
                }
            }
        }

    }

    //Method to Deccelerate
    void Brakes()
    {
        //Add Negative force to rigidbody
        //Reach 0 curSpeed, wait a given time, and then reverse
        if (verticalInput < 0)
        {
            if (curSpeed >= 0 && !reversing)
            {
                curSpeed = curSpeed - (accel * .10f) / 60;
            }

        }

        if (curSpeed <= 1 && verticalInput < 0)
        {
            reversing = true;
        }
        else if (verticalInput >= 0)
        {
            reversing = false;
        }
    }

    void Reverse()
    {
        if (verticalInput < 0)
        {
            if (curSpeed < reverseSpeed)
            {
                kart.AddForce(transform.forward * (-accel * .45f), ForceMode.Acceleration);
            }
            Mathf.Clamp(curSpeed, 0, reverseSpeed);
        }
    }

    //Method to Turn
    void Turn()
    {
        #region Deprecated Animator Controls
        //Turning animation
        if (horizontalInput > 0)
        {
            characterAnimator.SetBool("RightTurn", true);
            characterAnimator.SetBool("LeftTurn", false);
        }
        else if (horizontalInput < 0)
        {
            characterAnimator.SetBool("RightTurn", false);
            characterAnimator.SetBool("LeftTurn", true);
        }
        else
        {
            characterAnimator.SetBool("RightTurn", false);
            characterAnimator.SetBool("LeftTurn", false);
        }
        #endregion

        //Rotate object and change forward direction
        //if (curSpeed > 1)
        if (horizontalInput != 0)
        {
            foreach (Transform wheels in frontWheels) //Hard Coded, Can be smarter
            {
                wheels.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + ((turningRate - 20) * horizontalInput), 90);
                frontAxle.rotation = wheels.rotation;
            }
        }

        if (/*verticalInput > 0 &&*/ curSpeed > 1 && !reversing)
        {
            kart.transform.Rotate(0, Vector3.Angle(kart.transform.forward, frontAxle.forward) * horizontalInput / 30, 0);
        }
        else if (verticalInput < 0 && curSpeed > 1 && reversing)
        {
            kart.transform.Rotate(0, -Vector3.Angle(kart.transform.forward, frontAxle.forward) * horizontalInput / 30, 0);
        }
    }

    //Method to Drift
    void Drift()
    {
        //Rotate Kart Body based on Input and data from Rigidbody
        //Hold Button to Drift, Release to stop drifting
        if (difference < 45) //Literally the laziest. SMH LOL
        {

            kart.transform.Rotate(new Vector3(0, kart.transform.forward.y * horizontalInput + turningRate , 0), horizontalInput * turningRate * 2 * Time.deltaTime);
        }
    }

    //Method to add Boost!
    void Boost()
    {
        //accelMod = accel;
    }

    //Method to add acceleration to all karts not in 1st
    void CatchUp()
    {
        //Take in the distance to 1st Place (In positions) and assign a modifier to the acceleration
    }

    void Attack()
    {
        //Slow down a person if Raycast detects a Kart
        float distance = 20;
        RaycastHit hit;
        Ray attackRay = new Ray(this.transform.position + new Vector3(0, 1), transform.forward * distance);
        Debug.DrawRay(this.transform.position + new Vector3(0, 1), transform.forward * distance);
        if (Physics.Raycast(attackRay, out hit, distance, Physics.IgnoreRaycastLayer)) 
        {
            Debug.Log("I see you " + hit.transform.name);
            if (Input.GetKeyDown(KeyCode.G))
            {
                hit.transform.GetComponent<AIController>().boostMod = -15;
            }
        }
    }
    #endregion
}
