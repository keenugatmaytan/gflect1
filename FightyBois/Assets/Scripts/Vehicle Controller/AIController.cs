﻿using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

//Current Issues
//1. 

public class AIController : Agent
{
    #region Variables
    #region Kart Components
    [Header("Kart Components")]
    [SerializeField]
    Rigidbody kart; //Reference to main kart object's rigidbody
    [SerializeField]
    Transform kartBody; //Reference to kart model's Transform
    [SerializeField]
    Transform[] frontWheels; //Reference to front wheels for visual purposes (Not needed for current design)
    [SerializeField]
    Transform frontAxle;
    [SerializeField]
    Material[] Textures;
    [SerializeField]
    MeshRenderer[] kartParts;
    [SerializeField]
    GameObject[] drivers;
    [SerializeField]
    VehicleStats[] vehicle;
    public Animator characterAnimator; //Reference to character animator (Not needed for current design)
    #endregion

    #region Kart Specs Variabless
    [Header("Kart Specs Variables")]
    //[SerializeField]
    //bool isForward; //Direction Value
    [SerializeField]
    public bool grounded = false; //Determines if the car is grounded and oriented properly
    [SerializeField]
    [Range(20, 100)]
    public float accel = 20; //Acceleration Value
    [SerializeField]
    AnimationCurve accelCurve;
    [SerializeField]
    float accelCurveVal = 0;
    [SerializeField]
    float finalAccelVal = 0;
    [SerializeField]
    float accelTimer = 0;
    [SerializeField]
    public float boostMod = 0; //Boost Value (Gets added after a drift)
    [SerializeField]
    float atkMod = 0;
    [SerializeField]
    float boost = 3; //Countdown for Boost Decay (Decay Boost after 3(or boost) seconds
    [SerializeField]
    public float boostCool = 3; //Cooldown for the Boost Ability
    [SerializeField]
    float ouch = 2; //Countdown for Attack Decay
    [SerializeField]
    public float ouchCool = 3; //Cooldwon for the Attack Ability
    [SerializeField]
    [Range(20, 50)]
    float topSpeed = 20; //Max Forward Speed
    [SerializeField]
    float reverseSpeed; // Max Backward Speed
    [SerializeField]
    public float curSpeed; //Current Speed
    [SerializeField]
    [Range(45, 100)]
    float turningRate = 45; //Turning Rate (How much the object will rotate along the y axis per physics second)
    [SerializeField]
    Mine bomb, cig;
    [SerializeField]
    Abilities abilities; //Reference to Abilities Scriptable Object
    [SerializeField]
    int charSelect = 0; //Temporary Variable to test abilities
    [SerializeField]
    public float abilityCool = 0;
    [SerializeField]
    public int essAmt = 0; //Current Amount of Essence
    [SerializeField]
    int charIndex;
    [SerializeField]
    Animator disable; //Animation Clip for Duwende's Disable Animation
    #endregion

    #region Inputs
    [Header("Inputs")]
    [SerializeField]
    float verticalInput, horizontalInput;
    [SerializeField]
    bool turn = true;
    [SerializeField]
    bool drifting = false;
    [SerializeField]
    bool reversing = false;
    [SerializeField]
    bool slow = false; //Boolean for checking if currently affected by attack
    [SerializeField]
    bool fast = false; //Boolean for checking if currently boosting
    DualShock4Input controllerInput;
    public bool player;

    private Dictionary<string, KeyCode> keybind = new Dictionary<string, KeyCode>();
    public List<KeyCode> myKeys = new List<KeyCode>();
    #endregion

    #region UIVariables
    public cooldown[] skillCD;
    public Image test;
    #endregion

    #region Visual Effects
    [SerializeField]
    ParticleSystem[] driftSmoke;
    [SerializeField]
    ParticleSystem[] fireBoost;
    [SerializeField]
    ParticleSystem[] grayBoost;
    [SerializeField]
    ParticleSystem stars;
    [SerializeField]
    ParticleSystem[] ultSmoke;
    #endregion

    #region Debug
    [Header("Debug")]
    [SerializeField]
    float kartBodyY;
    [SerializeField]
    float kartRBY;
    [SerializeField]
    float difference;
    [SerializeField]
    float angle;
    [SerializeField]
    Transform resetPosition;
    #endregion
    #endregion
    float timer = 0;
    float previousDist = 0;

    #region Unity Functions
    private void Start()
    {
        #region Character Selection

            #region Art Stuff
            charSelect = gamemanager.instance.charSelect[charIndex];
            foreach (var item in kartParts)
            {
                item.material = Textures[charSelect];
            }
            drivers[charSelect].SetActive(true);
        #endregion
        #region Code Stuff
        accel = vehicle[charSelect].accel;
        accelCurve = vehicle[charSelect].accelCurve;
        topSpeed = vehicle[charSelect].topSpeed;
        turningRate = vehicle[charSelect].turningRate;
        #endregion
        #endregion

        #region Keybind Block
        keybind.Add("forward", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("forward", "C")));
        keybind.Add("left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left", "LeftArrow")));
        keybind.Add("right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right", "RightArrow")));
        keybind.Add("reverse", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("reverse", "X")));
        keybind.Add("drift", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("drift", "Z")));
        keybind.Add("ult", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ult", "Space")));
        keybind.Add("A1", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A1", "A")));
        keybind.Add("A2", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A2", "S")));

        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("forward", "C")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left", "LeftArrow")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right", "RightArrow")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("reverse", "X")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("drift", "Z")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ult", "Space")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A1", "A")));
        myKeys.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("A2", "S")));
        #endregion

    }

    // Update is called once per frame
    void Update()
    {
        kartBodyY = kartBody.eulerAngles.y;
        kartRBY = kart.transform.eulerAngles.y;
        difference = Mathf.Abs(kartRBY - kartBodyY);
        angle = kart.transform.forward.y - kartBody.eulerAngles.y;
        curSpeed = kart.velocity.magnitude;

        #region Abilitiy Effects
        if (slow)
        {
            ouch -= Time.deltaTime;
            if (ouch <= 0)
            {
                atkMod = 0;
                slow = false;
            }
        }

        if (fast)
        {
            switch (charSelect)
            {
                case 1:
                    foreach (var item in fireBoost)
                    {
                        Debug.Log("Boost");
                        item.Play();
                    }
                    break;
                default:
                    foreach (var item in grayBoost)
                    {
                        Debug.Log("Boost");
                        item.Play();
                    }
                    break;
            }
            boostMod = accel;
            boost -= Time.deltaTime;
            if (boost <= 0)
            {
                fast = false;
                boostMod = 0;
                boost = 3;
            }
        }
        else
        {
            switch (charSelect)
            {
                case 1:
                    foreach (var item in fireBoost)
                    {
                        item.Stop(false, ParticleSystemStopBehavior.StopEmitting);
                    }
                    break;
                default:
                    foreach (var item in grayBoost)
                    {
                        item.Stop(false, ParticleSystemStopBehavior.StopEmitting);
                    }
                    break;
            }
        }
        #endregion

        #region Accel Curves

        if (verticalInput > 0)
        {
            Debug.Log("Vroom");
            //Accelerate Block
            //if (accelTimer < accelCurve.keys.time)

            accelTimer += Time.deltaTime;
            accelCurveVal = accelCurve.Evaluate(accelTimer);
        }
        else if (accelCurveVal > 0 && verticalInput == 0 && !reversing)
        {
            //No Forward Input
            accelCurveVal -= 13 * Time.deltaTime;
            accelTimer = 0;
        }
        else if (verticalInput < 0 && accelCurveVal > 0 && !reversing)
        {
            //Braking
            accelCurveVal -= 15 * Time.deltaTime;
            accelTimer = 0;
        }
        //else if (verticalInput < 0 && reversing)
        //{
        //    //Reverse
        //    accelCurveVal = accelCurve.keys[0].value;
        //    accelTimer = 0;
        //}
        //else if (verticalInput == 0 && reversing)
        //    //No Reverse Input
        //    accelCurveVal -= 17 * Time.deltaTime;
        //    accelTimer = 0;

        //if (accelCurveVal < 50)
        //    //No usable force below certain treshold
        //    accelCurveVal = 0;
        #endregion
    }

    //Essence Collision Logic
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            //SetReward(-1.0f);
            //EndEpisode();
        }

        if (collision.gameObject.tag == "Respawn")
        {
            //SetReward(-1.0f);
            EndEpisode();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(kartBody.position, -kartBody.up);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Essence"))
        {
            other.gameObject.GetComponent<SphereCollider>().enabled = false;
            Debug.Log("Essence: " + other.gameObject.name);
            essAmt += 1;
            other.gameObject.GetComponent<Essence>().reArm = true;
        }

        if (other.CompareTag("Boost"))
        {
            fast = true;
            kart.AddForce(transform.forward * topSpeed * 30f, ForceMode.Impulse);
        }
    }
    #endregion

    #region MLAgents Functions
    public override void Initialize()
    {
        kart = this.GetComponent<Rigidbody>();
        reverseSpeed = topSpeed * .35f;

        if(GetComponent<DualShock4Input>() != null)
            controllerInput = GetComponent<DualShock4Input>();
    }

    public override void OnEpisodeBegin()
    {
        if (GetComponent<Position>().targetIndex != 0)
            this.transform.position = GetComponent<Position>().checkpoint.checkpoints[GetComponent<Position>().targetIndex - 1].transform.position;
        this.transform.LookAt(GetComponent<Position>().checkpoint.checkpoints[GetComponent<Position>().targetIndex].transform.position);
        previousDist = GetComponent<Position>().distance;
        timer = 0;
        GetComponent<Position>().timer = 10;
       /* GetComponent<Position>().targetIndex = 0;
        GetComponent<Position>().lap = 1;
        GetComponent<Position>().target = GetComponent<Position>().checkpoint.checkpoints[GetComponent<Position>().targetIndex].transform;*/
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        //Way too many if statements to be proper

        //Take Input from player
        verticalInput = vectorAction[0];
        horizontalInput = vectorAction[1];

        if (grounded)
        {
            Accelerate();
            Brakes();
            Reverse();
            Abilities();
        }

        if (grounded && turn)
        {
            Turn();
        }

        #region Commented Ability Code
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    boostCool -= Time.deltaTime;
        //    if(boostUI != null)
        //        boostUI.color = Color.red;

        //    if (boostCool <= 0)
        //    {
        //        boostCool = 3;
        //        if (boostUI != null)
        //            boostUI.color = Color.green;
        //        fast = true;
        //    }
        //}
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    ouchCool -= Time.deltaTime;
        //    if(attUI != null)
        //        attUI.color = Color.red;

        //    if (ouchCool <= 0)
        //    {
        //        ouchCool = 3;
        //        if (attUI != null)
        //            attUI.color = Color.green;
        //        Attack();
        //    }
        //}
        #endregion

        //The Car should auto reset to facing forward after drifting (Reset kartBody's forward to be the same as kart's forward)
        if (difference > 0 && !drifting)
        {
            kartBody.Rotate(new Vector3(0, kart.transform.eulerAngles.y - kartBody.eulerAngles.y), 45 * Time.deltaTime);
        }

        if (vectorAction[2] != 0)
        {
            drifting = true;
            foreach (var item in driftSmoke)
            {
                item.Play();
            }
            Drift();
        }
        else
        {
            foreach (var item in driftSmoke)
            {
                item.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
            drifting = false;
        }

        #region Training Code
        /*if (curSpeed < 10)
        {
            timer += Time.deltaTime;
            SetReward(timer * -0.1f);

            if (timer >= 10)
            {
                SetReward(-1.0f);
                timer = 0;
                EndEpisode();
            }
        }

        else if (curSpeed >= 30)
        {
            timer = 0;
            SetReward(2.0f);
        }

        else
        {
            timer = 0;
            SetReward(curSpeed * 0.1f);
        }

        if (previousDist >= GetComponent<Position>().distance)
        {
            SetReward(0.1f);
            previousDist = GetComponent<Position>().distance;
        }*/
        #endregion
    }

    public override void Heuristic(float[] actionsOut)
    {
        //actionsOut[0] = Vertical Axis
        //actionsOut[1] = Horizontal Axis
        //actionsOut[2] = Drift

        if (Input.GetKey(keybind["forward"]))
            actionsOut[0] = 1;
        else if (Input.GetKey(keybind["reverse"]))
            actionsOut[0] = -1;
        else
            actionsOut[0] = 0;

        if (Input.GetKey(keybind["left"]))
        {
            actionsOut[1] = -1;
        }
        else if (Input.GetKey(keybind["right"]))
        {
            actionsOut[1] = 1;
        }
        else
        {
            actionsOut[1] = 0;
        }

        if (controllerInput != null)
            actionsOut[1] = Input.GetAxis("Horizontal" + "_" + (int)controllerInput._joystickPlayerNum);       

        if (Input.GetKey(keybind["drift"]))
            actionsOut[2] = 1;
        else
            actionsOut[2] = 0;
    }
    #endregion

    #region Custom Functions
    #region Movement

    /// <summary>
    /// Adds forward force to simulate acceleration
    /// </summary>
    void Accelerate()
    {
        //Add force to rigidbody
        if (curSpeed <= topSpeed + (topSpeed * .10f))
        {
            //Boost value adds force to kart regardless of input
            kart.AddForce(transform.forward * boostMod, ForceMode.Acceleration);
        }

        //if (controllerInput != null)
        //{
        //    if (curSpeed <= topSpeed && (verticalInput > 0 || controllerInput.GetButton(ControlCode.R2)))
        //    {
        //        Debug.Log("boop");
        //        //kart.AddForce(transform.forward * (finalAccelVal  + atkMod));
        //        curSpeed += finalAccelVal;
        //    }
        //}

        //else
        {
            if (curSpeed <= topSpeed && verticalInput > 0)
            {
                kart.AddForce(transform.forward * (accelCurveVal + atkMod), ForceMode.Acceleration);
            }
            else if (verticalInput == 0)
            {
                kart.AddForce(transform.forward * accelCurveVal, ForceMode.Acceleration);
            }
        }

    }

    //Method to Deccelerate
    void Brakes()
    {
        #region Commented Code
        //Add Negative force to rigidbody
        //Reach 0 curSpeed, wait a given time, and then reverse
        //if (controllerInput != null)
        //{
        //    if (verticalInput < 0 || controllerInput.GetButton(ControlCode.L2))
        //    {
        //        if (curSpeed >= 0 && !reversing)
        //        {
        //            kart.AddForce(transform.forward * accelCurveVal, ForceMode.Acceleration);
        //        }
        //    }

        //    if (curSpeed <= 1 && (verticalInput < 0 || controllerInput.GetButton(ControlCode.L2)))
        //    {
        //        reversing = true;
        //    }
        //    else if (verticalInput >= 0 || controllerInput.GetButton(ControlCode.R2))
        //    {
        //        reversing = false;
        //    }
        //}
        //else
        #endregion
        {
            if (verticalInput < 0)
            {
                if (curSpeed >= 0 && !reversing)
                {
                    kart.AddForce(transform.forward * accelCurveVal, ForceMode.Acceleration);
                }
            }

            if (verticalInput < 0 && curSpeed <=0)
            {
                reversing = true;
            }
            else if (verticalInput >= 0 && curSpeed <= 0)
            {
                reversing = false;
            }
        }
    }

    void Reverse()
    {
        if (verticalInput < 0 && reversing)
        {
            if (curSpeed < reverseSpeed)
            {
                kart.AddForce(transform.forward * -accelCurveVal, ForceMode.Acceleration);
            }
            Mathf.Clamp(curSpeed, 0, reverseSpeed);
        }
        else if (reversing && verticalInput == 0)
        {
            kart.AddForce(transform.forward * -accelCurveVal, ForceMode.Acceleration);
        }
    }

    //Method to Turn
    void Turn()
    {
        #region Animator Controls
        //Turning animation
        if (horizontalInput > 0)
        {
            characterAnimator.SetBool("RightTurn", true);
            characterAnimator.SetBool("LeftTurn", false);
        }
        else if (horizontalInput < 0)
        {
            characterAnimator.SetBool("RightTurn", false);
            characterAnimator.SetBool("LeftTurn", true);
        }
        else
        {
            characterAnimator.SetBool("RightTurn", false);
            characterAnimator.SetBool("LeftTurn", false);
        }
        #endregion

        //Rotate object and change forward direction
        //if (curSpeed > 1)
        if (horizontalInput != 0)
        {
            foreach (Transform wheels in frontWheels) //Hard Coded, Can be smarter
            {
                Debug.Log("Wheels Turning");
                wheels.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + ((turningRate - 20) * horizontalInput), 90);
                frontAxle.rotation = wheels.rotation;
            }
        }
        else
        {
            foreach (Transform wheels in frontWheels) //Hard Coded, Can be smarter
            {
                Debug.Log("Wheels Turning");
                wheels.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 90);
                frontAxle.rotation = wheels.rotation;
            }
        }

        if (/*verticalInput > 0 &&*/ curSpeed > 1 && !reversing)
        {
            kart.transform.Rotate(0, Vector3.Angle(kart.transform.forward, frontAxle.forward) * horizontalInput / 30, 0);
        }
        else if (verticalInput < 0 && curSpeed > 1 && reversing)
        {
            kart.transform.Rotate(0, -Vector3.Angle(kart.transform.forward, frontAxle.forward) * horizontalInput / 30, 0);
        }
    }

    /// <summary>
    /// Allows the Kart to add angle to turning
    /// </summary>
    void Drift()
    {
        //Rotate Kart Body based on Input and data from Rigidbody
        //Hold Button to Drift, Release to stop drifting
        if (difference < 45)
        {
            kart.transform.Rotate(new Vector3(0, kart.transform.forward.y * horizontalInput + turningRate, 0), horizontalInput * turningRate * 2 * Time.deltaTime);
        }
    }
    #endregion

    void Abilities()
    {
        //Kapre = 1, Duwende = 0
        if (abilityCool <= 0)
        {
            if (Input.GetKeyDown(keybind["A1"]) && player) //Get input from player
            {
                fast = true; //Activate boost
                skillCD[2].cdAmount = 6;
                skillCD[3].cdAmount = 6;
                abilityCool = 6;
            }
            if (Input.GetKeyDown(keybind["A2"]) && player)
            {
                switch (charSelect) //Activate Ability based on chosen character
                {
                    case 1:
                        abilities.ReduceAccel(cig, transform);
                        skillCD[1].cdAmount = 6;
                        abilityCool = 6;
                        break;
                    default:
                        abilities.SpinAttack(bomb, transform);
                        skillCD[1].cdAmount = 6;
                        abilityCool = 6;
                        break;
                }
            }
            if (Input.GetKeyDown(keybind["ult"]) && player)
            {
                switch (charSelect)
                {
                    case 1:
                        if (essAmt >= 10)
                        {
                            essAmt -= 10;
                            skillCD[0].cdAmount = 6;
                            abilityCool = 6;
                            foreach (var item in ultSmoke)
                            {
                                item.Play();
                            }
                        }
                        break;
                    default:
                        abilities.ResetCoolDowns(this.GetComponent<AIController>(), essAmt, 10);
                        break;
                }
            }
        }else 
        {
            abilityCool -= Time.deltaTime;
            if (abilityCool <= 0)
            {
                abilityCool = 0;
            }
        }
    }

    public void GetHit(int atk)
    {
        stars.Play();
        switch (atk)
        {
            case 1:
                slow = true;
                atkMod = -accel * .75f;
                //stars.Stop(false, ParticleSystemStopBehavior.StopEmitting);
                break;
            default:
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                disable.Play("CartDisable");
                //stars.Stop(false, ParticleSystemStopBehavior.StopEmitting);
                break;
        }
    }

    #endregion
}
