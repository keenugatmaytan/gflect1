﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour
{
    public string[] trackNames;
    [SerializeField]
    GameObject loadingScreen;
    [SerializeField]
    Image progress;
    public Image loadingBG;
    public Sprite[] mapPics;
    List<AsyncOperation> scenesLoading = new List<AsyncOperation>();
    public void loadTrack(int i) {
        loadingScreen.SetActive(true);
        if (i == 2) {
            loadingBG.sprite = mapPics[0];
        } else
            loadingBG.sprite = mapPics[1];
        scenesLoading.Add(SceneManager.LoadSceneAsync(i, LoadSceneMode.Single));
        StartCoroutine(GetLoadProg());
    }

    public void quitGame() {
        Application.Quit();
    }

    public IEnumerator GetLoadProg()
    {
        for (int i = 0; i < scenesLoading.Count; i++)
        {
            while (!scenesLoading[i].isDone)
            {
                progress.fillAmount = scenesLoading[i].progress;

                yield return null;
            }
        }

        loadingScreen.SetActive(false);
    }
}
