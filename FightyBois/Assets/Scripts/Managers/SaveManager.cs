﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class SaveManager : MonoBehaviour
{
    public Data data;

    private string file = "playerdata.txt";

    public void Save(Data savedata)
    {
        string json = JsonUtility.ToJson(savedata);
        WriteToFile(file, json);
    }

    public void Load()
    {
        data = new Data();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, data);
        gamemanager.instance.Load(data);
    }

    private void WriteToFile(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }

    public string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }

    private string ReadFromFile(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
        {
            Debug.Log("File not found!");
            gamemanager.instance.myData = new Data();
        }
        return "";
    }
}
