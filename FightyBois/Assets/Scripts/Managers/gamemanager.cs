﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class gamemanager : MonoBehaviour
{
    public static gamemanager instance = null;
    public Data myData;
    public SaveManager saveManager;
    public int cpu = 0;
    public int laps = 1, players = 1, playersPicked = 0;
    public bool abilities = true;
    public bool raceMode;
    public int selectedMap;
    public List<Leaderboard> beachLeaderboard = new List<Leaderboard>();
    public List<Leaderboard> lakeLeaderboard = new List<Leaderboard>();
    public int[] charSelect;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(gameObject);
        saveManager.Load();
    }

    public void updateCharSize()
    {
        charSelect = new int[charSelect.Length + cpu];
    }

    public void TimeTrial(Leaderboard player)
    {
        if (selectedMap == 2)
        {
            int n = beachLeaderboard.Count;
            if (n >= 8 && beachLeaderboard[n - 1].time > player.time)
            {
                beachLeaderboard.Remove(beachLeaderboard[n - 1]);
                beachLeaderboard.Add(player);
            }
            else if (n < 8)
            {
                beachLeaderboard.Add(player);
            }
            beachLeaderboard.Sort(SortByTime);
            for (int i = 0; i < beachLeaderboard.Count; ++i)
            {
                myData.time[i] = beachLeaderboard[i].time;
                myData.character[i] = beachLeaderboard[i].character;
            }
        }
        else if (selectedMap == 3)
        {
            int n = lakeLeaderboard.Count;
            if (n >= 8 && lakeLeaderboard[n - 1].time > player.time)
            {
                lakeLeaderboard.Remove(lakeLeaderboard[n - 1]);
                lakeLeaderboard.Add(player);
            }
            else if (n < 8)
            {
                lakeLeaderboard.Add(player);
            }
            lakeLeaderboard.Sort(SortByTime);
            for (int i = 8; i < lakeLeaderboard.Count + 8; ++i)
            {
                myData.time[i] = lakeLeaderboard[i-8].time;
                myData.character[i] = lakeLeaderboard[i-8].character;
            }
        }
        Save();
        /*int n = leaderboardTime.Length;
        float temp;
        leaderboardTime[n-1] = Mathf.Min(leaderboardTime[n], time);
        for (int i = 0; i < n; ++i)
        {
            for (int j = i + 1; j < n; ++j)
            {
                if (leaderboardTime[i] < leaderboardTime[j])
                {
                    temp = leaderboardTime[i];
                    leaderboardTime[i] = leaderboardTime[j];
                    leaderboardTime[j] = temp;
                }
            }
        }*/
    }
    private int SortByTime(Leaderboard p1, Leaderboard p2)
    {
        return p1.time.CompareTo(p2.time);
    }

    void Save()
    {
        saveManager.Save(myData);
    }

    public void Load(Data data)
    {
        myData = data;
        for(int i = 0; i < myData.time.Length; ++i)
        {
            Leaderboard temp = new Leaderboard();
            temp.time = myData.time[i];
            temp.character = myData.character[i];
            if (myData.time[i] > 0)
            {
                if (i < 8)
                {
                    beachLeaderboard.Add(temp);
                }
                else
                {
                    lakeLeaderboard.Add(temp);
                }
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
            Save();
    }

    private void OnApplicationQuit()
    {
        Save();
    }
}
