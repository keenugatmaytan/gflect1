﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelManager : MonoBehaviour
{
    public GameObject[] otherKarts;

    // Start is called before the first frame update
    void Start()
    {
        if (!gamemanager.instance.raceMode && otherKarts.Length != 0) {
            for (int i = 0; i < 7; i++) {
                otherKarts[i].gameObject.SetActive(false);
            }
        }
    }

    public void loadMainMenu() {
        SceneManager.LoadScene("Title Screen");
    }
}
