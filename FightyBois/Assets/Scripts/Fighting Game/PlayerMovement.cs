﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private DualShock4Input ps;

    private Animator playerAnimator;
    private SpriteRenderer playerSprite;

    private Rigidbody _body;
    private int _jumpCount;
    private Vector3 _myVelocity;

    public Transform attackSpawn;
    public GameObject punch;
    public float jumpForce;
    public float speed;
    public float gravity;

    public PlayerType playerType;

    private void Start()
    {
        playerAnimator = GetComponent<Animator>();
        playerSprite = GetComponent<SpriteRenderer>();
        ps = GetComponent<DualShock4Input>();
        _jumpCount = 1;
        _body = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        Attack();

    }
    private void FixedUpdate()
    {
        CheckGround();
        _myVelocity = _body.velocity;
        Move();

        if ((ps.GetAxisRaw(ControlCode.DpadY) == 1) || (ps.GetAxisRaw(ControlCode.LeftStickY) == 1) || Input.GetKeyDown(KeyCode.Space) && _jumpCount > 0)
        {
            Jump();
        }

        ModifyGravity();
    }

    void Move()
    {
        float _directionX = Input.GetAxisRaw("Horizontal");

        if (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Punch"))
        {
            if (ps.GetAxisRaw(ControlCode.LeftStickX) < 0 || _directionX == -1)
            {
                playerSprite.flipX = true;
                attackSpawn.localPosition = new Vector2(-.25f, attackSpawn.localPosition.y);
            }
            else if (ps.GetAxisRaw(ControlCode.LeftStickX) > 0 || _directionX == 1)
            {
                playerSprite.flipX = false;
                attackSpawn.localPosition = new Vector2(.25f, attackSpawn.localPosition.y);
            }

            if (_directionX != 0 || ps.GetButton(ControlCode.LeftStickX) || ps.GetButton(ControlCode.DpadX))
            {
                playerAnimator.SetBool("Walk", true);
                _body.velocity = new Vector2(_directionX * speed, _myVelocity.y);
            }
            else
            {
                playerAnimator.SetBool("Walk", false);
                _body.velocity = new Vector2(0, _myVelocity.y);
            }
        }
    }

    void Attack()
    {
        if (ps.GetButtonDown(ControlCode.Square) || Input.GetKeyDown(KeyCode.F))
        {
            playerAnimator.SetBool("Walk", false);
            playerAnimator.SetTrigger("Punch");
            GameObject obj = Instantiate(punch, attackSpawn.position, punch.transform.rotation);
        }
    }

    void ModifyGravity()
    {
        _body.AddForce(new Vector2(0, -gravity));
    }

    void Jump()
    {
        _body.velocity = new Vector3(_myVelocity.x, jumpForce, 0);
        --_jumpCount;
    }

    void CheckGround()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position, Vector3.down * transform.localScale.y/2,Color.red);
        if (Physics.Raycast(transform.position, Vector3.down, out hit, transform.localScale.y / 2, LayerMask.GetMask("Ground")))
        {
            if (hit.collider.CompareTag("Floor"))
            {
                _jumpCount = 1;
                Debug.Log("GROUNDED");
            }
        }
        else
        {
            _jumpCount = 0;
            Debug.Log("NOT GROUND");
        }
    }
}
