﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    GameObject objectToFollow;
    [SerializeField]
    Transform positionToBe;
    [SerializeField]
    Vector3 followOffset = new Vector3(0, 2, -5);
    [SerializeField]
    float cameraDelay = .5f;

    private void FixedUpdate()
    {
        this.gameObject.transform.position = Vector3.Lerp(this.transform.position,(positionToBe.position), cameraDelay);
        this.transform.LookAt(objectToFollow.GetComponentInParent<Transform>().position + followOffset, objectToFollow.transform.up);
    }
}
