﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Essence : MonoBehaviour
{
    [SerializeField]
    float coolDown = 3;
    public bool reArm = false;
    [SerializeField]
    ParticleSystem essence, beam, sparks;

    private void Update()
    {
        if (reArm)
        {
            ResetObject();
        }
    }
    public void ResetObject()
    {
        essence.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        beam.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        sparks.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        coolDown -= Time.deltaTime;
        if (coolDown <= 0)
        {
            reArm = false;
            this.GetComponent<SphereCollider>().enabled = true;
            coolDown = 3;
            essence.Play();
            beam.Play();
            sparks.Play();
        }
    }
}
