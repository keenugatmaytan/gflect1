﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position : MonoBehaviour
{
    public string text;
    public Checkpoints checkpoint;
    public int targetIndex;
    public int lap;
    public int checkpointCount;
    public Transform target;
    public int place;
    public bool addLap;
    public float distance;
    public bool finish;
    public updateLap lapTrackerUI, ttLapTracker;
    public GameObject GameOverUI, leaderboardUI;
    public float timer = 10;
    public bool startTimer;
    public timer myTimer;
    public Leaderboard player = new Leaderboard();
    

    private void Start()
    {
        targetIndex = 0;
        lap = 1;
        checkpoint = FindObjectOfType<Checkpoints>().GetComponent<Checkpoints>();
        checkpointCount = checkpoint.checkpoints.Length;
        target = checkpoint.checkpoints[targetIndex].transform;

        //enables time trial timer
        startTimer = false; 
        player.time = 0f;
    }

    private void Update()
    {
        distance = Vector3.Distance(target.position, transform.position);
        /*timer -= Time.deltaTime;

        if (timer <= -10)
        {
            GetComponent<AIController>().SetReward(timer * 0.01f);
            timer = 10;
            GetComponent<AIController>().EndEpisode();
        }*/
        if (startTimer)
        {
            player.time += Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        if (!gamemanager.instance.raceMode && finish == false)
        {
            player.time += Time.deltaTime;
            myTimer.DisplayTime(player.time);
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Checkpoint") || other.gameObject.CompareTag("FinishLap")) && other.transform == target)
        {
            //GetComponent<AIController>().SetReward(timer * 0.1f);
            //timer = 10;
            //Debug.Log("Checkpoint Hit!");

            if (targetIndex < checkpointCount - 1)
            {
                targetIndex++;
                target = checkpoint.checkpoints[targetIndex].transform;
            }
            else
            {
                addLap = true;
                targetIndex = 0;
                target = checkpoint.checkpoints[targetIndex].transform;
            }
            if (other.CompareTag("FinishLap"))
            {
                if (!gamemanager.instance.raceMode)
                {
                    //compares time trial time to gamemanager leaderboard array
                    if (lap == gamemanager.instance.laps)
                    {
                        player.character = gamemanager.instance.charSelect[0];
                        gamemanager.instance.TimeTrial(player);
                        Debug.Log("TIME: " + player.time + "CHAR: " + player.character);
                        leaderboardUI.SetActive(true);
                    }
                    //enables time trial timer after touching starting lap once
                    else
                    {
                        startTimer = true;
                    }
                }
                if (addLap)
                {
                    Debug.Log("lapdone");
                    if (lap == gamemanager.instance.laps)
                    {
                        finish = true;
                        this.GetComponent<AIController>().enabled = false;
                        checkpoint.AddPlace(this.GetComponent<Position>());
                        if (GameOverUI != null && gamemanager.instance.raceMode)
                            GameOverUI.SetActive(true);      
                    }
                    else
                    {
                        ++lap;
                        if (gamemanager.instance.raceMode && lapTrackerUI != null)
                        {
                            lapTrackerUI.updateText(lap);
                        }
                        else if (ttLapTracker != null)
                            ttLapTracker.updateText(lap);
                    }
                    addLap = false;
                }
            }
            //GetComponent<AIController>().EndEpisode();
        }
        /*if (other.gameObject.CompareTag("Out"))
        {
            Vector3 _pos = checkpoint.checkpoints[targetIndex - 1].transform.position;
            transform.position = new Vector3(_pos.x, _pos.y + transform.localScale.y, _pos.z);
        }*/
    }
}
