﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitScreen : MonoBehaviour
{
    [SerializeField]
    Camera[] playerCams;
    [SerializeField]
    int numActiveCams;

    // Update is called once per frame
    void Update()
    {
        InvokeRepeating("CheckActiveCams", 0, 3);
    }


    /// <summary>
    /// Regularly Checks for number of active Cams
    /// </summary>
    void CheckActiveCams()
    {
        numActiveCams = FindObjectsOfType<Camera>().Length;
        SetCameraPorts();
    }

    /// <summary>
    /// Sets the cameras to the proper viewport settings based on number of active player cameras and player settings.
    /// </summary>
    void SetCameraPorts()
    {
        switch (numActiveCams)
        {
            case 2:
                playerCams[0].pixelRect = new Rect(0, 0, Screen.width / 2, Screen.height);
                playerCams[1].pixelRect = new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height);
                break;

            case 3:
                playerCams[0].pixelRect = new Rect(0, Screen.height/2, Screen.width / 2, Screen.height);
                playerCams[1].pixelRect = new Rect(Screen.width / 2, Screen.height/2, Screen.width / 2, Screen.height);
                playerCams[2].pixelRect = new Rect(0, 0, Screen.width, Screen.height / 2);
                break;

            case 4:
                playerCams[0].pixelRect = new Rect(0, Screen.height / 2, Screen.width / 2, Screen.height);
                playerCams[1].pixelRect = new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height);
                playerCams[2].pixelRect = new Rect(0, 0, Screen.width / 2, Screen.height / 2);
                playerCams[3].pixelRect = new Rect(Screen.width / 2, 0, Screen.width, Screen.height / 2);
                break;

            default:
                playerCams[0].pixelRect = new Rect(0, 0, Screen.width, Screen.height);
                break;
        }
    }
}
