﻿using UnityEngine;

public class Mine : MonoBehaviour
{
    public int racer;

    private void OnCollisionEnter(Collision collision)
    {
        //If Kapre dropped mine, use the function that reduces acceleration
        //If Duwende dropped mine, play the animation
        if (collision.gameObject.GetComponent<AIController>())
        {
            collision.gameObject.GetComponent<AIController>().GetHit(racer);
        }
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        //If Kapre dropped mine, use the function that reduces acceleration
        //If Duwende dropped mine, play the animation
        Debug.Log("Mine hit by: " + other.gameObject.name);
        if (other.gameObject.GetComponentInParent<AIController>())
        {
            other.gameObject.GetComponentInParent<AIController>().GetHit(racer);
        }
        gameObject.SetActive(false);
    }
}
