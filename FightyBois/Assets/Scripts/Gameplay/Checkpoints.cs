﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    public GameObject[] checkpoints;
    public Position[] player;
    public List<Position> place = new List<Position>();
    public int playerCount;
    private Position temp;
    private int placeIndex = 0;
    public updatePosition positionUpdate;
    private void Start()
    {
        playerCount = player.Length;
    }
    private void FixedUpdate()
    {
        for (int i = 0; i < playerCount; ++i)
        {
            if (!player[i].finish)
            {
                for (int j = i + 1; j < playerCount; ++j)
                {
                    if (player[i].lap < player[j].lap)
                    {
                        temp = player[i];
                        player[i] = player[j];
                        player[j] = temp;
                    }
                    else if (player[i].lap == player[j].lap)
                    {
                        if (player[i].targetIndex < player[j].targetIndex && player[j].targetIndex != 0)
                        {
                            temp = player[i];
                            player[i] = player[j];
                            player[j] = temp;
                        }
                        else if (player[j].targetIndex == 0 && player[i].targetIndex > 0)
                        {
                            temp = player[i];
                            player[i] = player[j];
                            player[j] = temp;
                        }
                        else if (player[i].targetIndex == player[j].targetIndex)
                        {
                            if (player[i].distance > player[j].distance)
                            {
                                temp = player[i];
                                player[i] = player[j];
                                player[j] = temp;
                            }
                        }
                    }
                }
            }
            else
            { }
        }

        for (int x = 0; x < playerCount; ++x) {
            if (player[x].gameObject.tag == positionUpdate.gameObject.tag) {
                positionUpdate.updatePos(x + 1);
                //Debug.Log("curremt position = " + x + 1);
            }
        }
    }

    public void AddPlace(Position _player)
    {
        place.Add(_player);
    }
}
